<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelloWorldCommand extends Command
{
    public function __construct()
    {
        parent::__construct('hello:world');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Initializing...');
        
        sleep(2);
        
        $output->writeln('Hello world!');
        
        return 0;
    }
}
