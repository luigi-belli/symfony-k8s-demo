cache-clear-dev:
	@echo ""
	@echo "    ENV: DEV"
	@docker-compose exec -T fpm bin/console cache:clear --env=dev

cache-clear-test:
	@echo ""
	@echo "    ENV: TEST"
	@docker-compose exec -T fpm bin/console cache:clear --env=test

cache-warmup-dev:
	@echo ""
	@echo "    ENV: DEV"
	@docker-compose exec -T fpm bin/console cache:warmup --env=dev

cache-warmup-test:
	@echo ""
	@echo "    ENV: TEST"
	@docker-compose exec -T fpm bin/console cache:warmup --env=test
