export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="ys"
plugins=(git symfony2)
export PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/var/www/blaine/vendor/bin"
source $ZSH/oh-my-zsh.sh
